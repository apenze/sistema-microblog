<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/contatos', 'ContatosController@index')->name('contatos')->middleware('auth');

Route::get('/follow', 'ContatosController@follow')->middleware('auth');

Route::get('/unfollow', 'ContatosController@unfollow')->middleware('auth');

Route::post('/tweet', 'TweetsController@store')->middleware('auth');

Route::post('/retweet', 'TweetsController@retweet')->middleware('auth');

Route::get('/tweet/delete', 'TweetsController@delete')->middleware('auth');

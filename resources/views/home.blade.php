@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <form method="post" action="/tweet">
                        <div class="input-group">
                            {{ csrf_field() }}
                            <input name="descricao" type="text" class="form-control" placeholder="O que está acontecendo?">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <span class="glyphicon glyphicon-send" aria-hidden="true"></span>
                                </button>
                            </span>
                        </div>
                    </form>
                </div>
                <div class="list-group">
                    @foreach ($tweets as $tweet)
                        <a href="#" class="list-group-item">
                            @if ($tweet->tweet_id)
                                <strong>{{$tweet->tweet->user->name}}</strong> {{ $tweet->user->name }} retweeted
                                <p class="list-group-item-text">{{ $tweet->tweet->descricao }}</p>
                            @else
                                <strong>{{$tweet->user->name}}</strong>
                                <p class="list-group-item-text">{{ $tweet->descricao }}</p>
                            @endif
                            <form>
                                {{ csrf_field() }}
                                @if ($tweet->tweet_id)
                                    <button
                                        name="tweet_id"
                                        class="btn btn-link"
                                        type="submit"
                                        formmethod="post"
                                        formaction="/retweet"
                                        value="{{ $tweet->tweet->id }}">
                                        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                    </button>
                                @else
                                    <button
                                        name="tweet_id"
                                        class="btn btn-link"
                                        type="submit"
                                        formmethod="post"
                                        formaction="/retweet"
                                        value="{{ $tweet->id }}">
                                        <span class="glyphicon glyphicon-share-alt" aria-hidden="true"></span>
                                    </button>
                                @endif
                                @if (Auth::user()->name === $tweet->user->name)
                                    <button
                                        name="tweet_id"
                                        class="btn btn-link"
                                        type="submit"
                                        formmethod="get"
                                        formaction="/tweet/delete"
                                        value="{{ $tweet->id }}">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </button>
                                @endif
                            </form>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Contatos</div>
                <div class="list-group">
                    @foreach ($relacoes as $relacao)
                        <form method="GET" action="{{ url('/unfollow') }}">
                            <input type="hidden" name="id" value="{{ $relacao->contato->id }}">
                            <a href="#" class="list-group-item">
                                <strong>{{$relacao->contato->name}}</strong>
                                <button type="submit" class="btn btn-danger pull-right">unfollow</button>
                                <p class="list-group-item-text">{{ $relacao->contato->email }}</p>
                            </a>
                        </form>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Usuários</div>
                <div class="list-group">
                    @foreach ($usuarios as $contato)
                        <form method="GET" action="{{ url('/follow') }}">
                            <input type="hidden" name="id" value="{{ $contato->id }}">
                            <a class="list-group-item">
                                <strong>{{$contato->name}}</strong>
                                <button type="submit" class="btn btn-default pull-right">follow</button>
                                <p class="list-group-item-text">{{ $contato->email }}</p>
                            </a>
                        </form>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

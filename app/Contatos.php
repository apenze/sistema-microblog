<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contatos extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_contatos';

    /**
     * Get the user that owns the contato.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
    * Get the contato associated with the user.
    */
   public function contato()
   {
       return $this->belongsTo('App\User', 'contato_user_id');
   }
}

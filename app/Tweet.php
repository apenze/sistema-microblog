<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tweets';

    /**
     * Get the user that owns the tweet.
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
    * Get the tweet associated with the tweet.
    */
   public function tweet()
   {
       return $this->belongsTo('App\Tweet', 'tweet_id');
   }
}

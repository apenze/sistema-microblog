<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

use App\Http\Requests\StoreTweet;

use App\Tweet;

class TweetsController extends Controller
{
    public function store(StoreTweet $request)
    {
        $user = $request->user();

        $tweet = new Tweet;
        $tweet->user_id   = $user->id;
        $tweet->descricao = $request->input('descricao');

        $tweet->save();

        return redirect()->route('home');
    }

    public function retweet(Request $request)
    {
        $this->validate($request, [
            'tweet_id' => 'required',
        ]);

        $user = $request->user();

        $tweet = new Tweet;
        $tweet->user_id   = $user->id;
        $tweet->tweet_id = $request->input('tweet_id');

        $tweet->save();

        return redirect()->route('home');
    }

    public function delete(Request $request)
    {
        $this->validate($request, [
            'tweet_id' => 'required',
        ]);

        $tweet = Tweet::find($request->input('tweet_id'));
        $tweet->delete();

        return redirect()->route('home');
    }
}

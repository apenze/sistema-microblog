<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

use App\Contatos;

use App\User;

class ContatosController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = $request->user();

        $contatos = Contatos::with('contato')
            ->where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();

        $usuarios = User::orderBy('created_at', 'desc')
            ->whereDoesntHave('comments', function ($query) {
                $query->where('content', 'like', 'foo%');
            })
            ->get();

        return view('contatos')
            ->with([
                'relacoes' => $contatos,
                'usuarios' => $usuarios
            ]);
    }

    public function follow(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $user = $request->user();

        $contato = new Contatos;
        $contato->user_id = $user->id;
        $contato->contato_user_id = $request->input('id');

        $contato->save();

        return redirect()->route('contatos');
    }

    public function unfollow(Request $request)
    {
        $this->validate($request, [
            'id' => 'required',
        ]);

        $user = $request->user();

        $contatos = Contatos::where('user_id', $user->id)
            ->where('contato_user_id', $request->input('id'))
            ->get();
        
        $contatos->delete();

        return redirect()->route('contatos');
    }
}

sudo su

apt-get update

apt-get upgrade -y

composer selfupdate

apt-get update

chown -R www-data:www-data /var/www/

cd /var/www/

composer global update

composer global require "laravel/installer"

echo -e '\nexport PATH=$PATH:~/.composer/vendor/bin' >> ~/.bashrc

composer install -o

chmod -R 777 storage

php artisan migrate
